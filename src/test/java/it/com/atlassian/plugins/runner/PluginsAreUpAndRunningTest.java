package it.com.atlassian.plugins.runner;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertThat;

public class PluginsAreUpAndRunningTest extends BaseJiraFuncTest {
    private static final String[] PLUGIN_KEYS = {
            "com.atlassian.plugins.jira-agile-connect-plugin",
            "com.atlassian.upm.atlassian-universal-plugin-manager-plugin",
            "com.atlassian.plugins.atlassian-connect-plugin",
            "com.atlassian.jwt.jwt-plugin",
            "com.atlassian.bundles.json-schema-validator-atlassian-bundle"};

    @Test
    public void agileConnectPluginAreEnabled() throws IOException, InterruptedException {
        String property = System.getProperty("baseurl.jira");
        URI url = URI.create(property + "/rest/plugins/1.0/");

        HttpURLConnection connection = (HttpURLConnection) url.toURL().openConnection();
        connection.setRequestProperty("Authorization", "Basic " + new String(Base64.encodeBase64("admin:admin".getBytes())));
        connection.setRequestMethod("GET");

        String responseText = IOUtils.toString(connection.getInputStream());
        Set<String> installedPluginKeys = getInstalledPluginKeys(responseText);

        assertThat(installedPluginKeys, Matchers.hasItems(PLUGIN_KEYS));
    }

    private Set<String> getInstalledPluginKeys(String responseText) {
        JsonArray pluginArray = new JsonParser().parse(responseText).getAsJsonObject().get("plugins").getAsJsonArray();
        Set<String> plugins = new HashSet<String>();

        for (int i = 0; i < pluginArray.size(); i++) {
            JsonObject jsonObject = pluginArray.get(i).getAsJsonObject();
            plugins.add(getPluginKey(jsonObject));
        }
        return plugins;
    }

    private String getPluginKey(JsonObject plugin) {
        final JsonElement key = plugin.get("key");
        return key.getAsString();
    }
}
