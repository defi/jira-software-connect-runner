#!/bin/bash
## pushes changes to the repository
set -o nounset

# use gsed on the Mac
SED=sed
$(echo $OSTYPE | grep -q darwin) && SED=gsed

# needed by exit_with_error()
trap "exit 1" TERM
export PID=$$

function exit_with_error() {
    kill -s TERM $PID
}

function die() {
    local msg=${1:-}
    printf "Error running last command! %s\n" "$msg" >&2

    exit_with_error
}

function commitAll() {
    local msg=$1

    echo "Committing all changed files..."
    git commit -a -m "${msg}" || die
}

function addRemote() {
    local remote=$1
    local url=$2

    echo "Git remotes:"
    git remote -vv

    if [ `git remote | grep "${remote}$" | wc -l` -ne 0 ]; then
        echo "'${remote}' is already defined, deleting it"
        git remote rm ${remote}
    fi

    echo "Adding '${remote}' at ${url}"
    git remote add ${remote} "${url}" || die
}

function pushBranch() {
    local remote=$1
    local branch=${2:-}
    local useTags=${3:-''}

    echo "\n\nINFO: Displaying git symbolic-ref HEAD in order to make sure we are not in a detached head state.\n\n"

    git symbolic-ref HEAD
    if [ $? -ne 0 ];then
            echo "----------------------------------------------------------------------------------------------"
            echo ">>> FAILURE <<< We are in a detached head state.  Please fix or restart this failed build job."
            echo "----------------------------------------------------------------------------------------------"
            echo "\n\n\n"
            exit_with_error
    fi

    if [ -z "${branch}" ]; then
        echo "Pushing to ${remote}"
        git push -u ${useTags} ${remote} || die
    else
        echo "Pushing branch ${branch} to ${remote}"
        git push -u ${useTags} ${remote} ${branch} || die
    fi
}

function configureGitUser() {
    local name="JIRA Software Connect Runner Release Bot"
    local email="jira-software-connect-runner-release-bot@atlassian.com"

    echo "Setting name: ${name} <${email}>"
    git config user.name "${name}"
    git config user.email "${email}"
}
