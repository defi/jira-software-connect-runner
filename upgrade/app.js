#!/usr/bin/env node

var request = require('request'),
    xml2js = require('xml2js'),
    fs = require('fs'),
    _ = require('lodash');


var MANIFESTO_URL = "https://manifesto.atlassian.io/api/env/jirastudio-prd";
var POM_PATH = '../pom.xml';

var builder = new xml2js.Builder({renderOpts : {indent : '    ', pretty:true, 'newline': '\n' }});

function getVersions(artifacts, callback) {
    request(MANIFESTO_URL, {}, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            var obj = JSON.parse(body);
            var plugins = obj.details.plugins;
            var wanted = _.filter(plugins, function(plugin) {
                return plugin.product === 'jira' && _.contains(artifacts, plugin.artifact);
            });

            var jira = _.findWhere(obj.details.products, {artifact:'jira'});
            wanted.push(jira);
            callback(wanted);
        }
    });
}

function getArtifactsFromPom(callback) {
    fs.readFile('../pom.xml', 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        xml2js.parseString(data, function (err, result) {
            var properties = result.project.properties[0];

            Object.getOwnPropertyNames(properties).forEach(function(artifactName) {
                properties[artifactName] = properties[artifactName][0];
            });
            callback(properties);
        });
    });
}


function compareVersions(oldArtifacts, newArtifacts, sameCallback, differentCallback) {
    for (var i = 0; i < newArtifacts.length; i++) {
        var obj = newArtifacts[i];
        if (obj.version !== oldArtifacts[obj.artifact]) {
            differentCallback();
            return;
        }
    }
    sameCallback();

}

function generateNewPom(currentArtifacts, callback) {
    fs.readFile(POM_PATH, 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        xml2js.parseString(data, function (err, result) {
            var properties = result.project.properties[0];


            Object.getOwnPropertyNames(properties).forEach(function(artifactName) {
                var newObj = _.findWhere(currentArtifacts, {artifact : artifactName});
                if (newObj) {
                    properties[artifactName] = [newObj.version];
                }
            });


            var xml = builder.buildObject(result);
            callback(xml);
        });
    });
}

getArtifactsFromPom(function(oldArtifacts) {
    getVersions(_.keys(oldArtifacts), function(currentArtifacts) {
        compareVersions(oldArtifacts, currentArtifacts, function() {
            console.log("No changes");
            process.exit(1);
        }, function () {
            generateNewPom(currentArtifacts, function(xml) {
                fs.writeFile(POM_PATH, xml, function(err) {
                    if(err) {
                        return console.log(err);
                    }
                    console.log("Pom updated");
                    process.exit();
                });
            })
        });
    });
});


